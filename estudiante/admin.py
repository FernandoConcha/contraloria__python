from django.contrib import admin

# Register your models here.
from .models import *
from .forms import *



class AdminAlumno(admin.ModelAdmin):
        list_display = ["id","nombre","edad","carrera"]
        list_filter = ["id","fecha_ingreso","nombre"]
        forms = RegModelForm
        class Meta:
            model = Alumno

admin.site.register(Alumno,AdminAlumno)