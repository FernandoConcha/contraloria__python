from django.db import models

# Create your models here.
class Alumno(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre = models.CharField(max_length = 100, blank = False, null = False)
    edad = models.IntegerField(null = False)
    rut = models.IntegerField(null = False,blank = False)
    carrera = models.CharField(max_length = 100)
    fecha_ingreso = models.DateTimeField(auto_now_add = True, auto_now = False)

    def _str_(self):
        return self.nombre
