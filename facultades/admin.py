from django.contrib import admin

# Register your models here.
from .models import *
from .forms import *


class AdminFacultad(admin.ModelAdmin):
        list_display = ["id","nombre_facultad","ubicacion"]
        list_filter = ["id","nombre_facultad","ubicacion"]
        forms = RegModelForm
        class Meta:
            model = Facultad

admin.site.register(Facultad,AdminFacultad)