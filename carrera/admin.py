from django.contrib import admin

# Register your models here.
from .models import *
from .forms import *

class AdminCarrera(admin.ModelAdmin):
        list_display = ["id","nombre","facultad"]
        list_filter = ["id","fecha_apertura","nombre"]
        form = RegModelForm

        #class Meta:
         #   model = Carrera

admin.site.register(Carrera,AdminCarrera)