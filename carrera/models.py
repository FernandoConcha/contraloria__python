from django.db import models

# Create your models here.
class Carrera(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre = models.CharField(max_length = 100, blank = False, null = False)
    descripcion = models.CharField(max_length = 100)
    estudiante_min = models.IntegerField()
    facultad = models.CharField(max_length=100)
    fecha_apertura = models.DateTimeField(auto_now_add = True, auto_now = False)

    def _str_(self):
        return self.nombre
