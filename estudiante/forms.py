from django import forms
from .models import *

class RegModelForm(forms.ModelForm):
    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) >= 2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_estudiante(self):
        min = self.cleaned_data.get("edad")
        if min:
            if min >= 1 and min <= 17:
                return min
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_nombre_carrera(self):
        nombre = self.cleaned_data.get("carrera")
        if nombre:
            if len(nombre) >= 2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")