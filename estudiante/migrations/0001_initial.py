# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-17 16:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, unique=True)),
                ('nombre', models.CharField(max_length=100)),
                ('edad', models.IntegerField()),
                ('rut', models.IntegerField()),
                ('carrera', models.CharField(max_length=100)),
                ('fecha_ingreso', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
