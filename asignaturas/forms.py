from django import forms
from .models import *

class RegForm(forms.Form):
    nombre_asignatura = forms.CharField(max_length=100)
    nombre_carrera = forms.CharField(max_length=100)
    semestre = forms.IntegerField()

    class Meta:
        model = Asignatura
        fields = ["nombre_asignatura", "nombre_carrera", "semestre"]

    def clean_nombre_asignatura(self):
        nombre = self.cleaned_data.get("nombre_asignatura")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_nombre_carrera(self):
        nombre = self.cleaned_data.get("nombre_carrera")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
    def clean_semestre(self):
        min = self.cleaned_data.get("semestre")
        if min <= 0:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
        if min < 3:
            return min
        else:
            raise forms.ValidationError("El valor ingresado no es valido")


class RegModelForm(forms.ModelForm):
    class Meta:
        model = Asignatura
        fields = ["nombre_asignatura","nombre_carrera","semestre"]

    def clean_nombre_asignatura(self):
        nombre = self.cleaned_data.get("nombre_asignatura")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_nombre_carrera(self):
        nombre = self.cleaned_data.get("nombre_carrera")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
    def clean_semestre(self):
        min = self.cleaned_data.get("semestre")
        if min <= 0:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
        if min < 3:
            return min
        else:
            raise forms.ValidationError("El valor ingresado no es valido")