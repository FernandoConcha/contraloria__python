from django.db import models

# Create your models here.
class Facultad(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre_facultad = models.CharField(max_length = 100, blank = False, null = False)
    descripcion = models.CharField(max_length = 100)
    ubicacion = models.CharField(max_length=100)
    numero_min = models.IntegerField(null = False,blank = False)

    def _str_(self):
        return self.nombre_facultad
