from django.db import models

# Create your models here.
class Asignatura(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre_asignatura = models.CharField(max_length = 100, blank = False, null = False)
    nombre_carrera = models.CharField(max_length=100, blank=False, null=False)
    semestre = models.IntegerField(null=False, blank=False)
    fecha_incorporacion = models.DateTimeField(auto_now_add = True, auto_now = False)

    def _str_(self):
        return self.nombre_asignatura
