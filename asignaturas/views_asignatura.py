from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from .forms import RegForm
from .models import *

def inicioAsignatura(request):
    form = RegForm(request.POST or None)
    contexto = {
        "el_formulario1":
            form,
    }
    if form.is_valid():
        form_data = form.cleaned_data

        nombre_asignatura1 = form_data.get("nombre_asignatura")
        nombre_carrera1 = form_data.get("nombre_carrera")
        semestre1 = form_data.get("semestre")
        objeto = Asignatura.objects.create(nombre_asignatura=nombre_asignatura1, nombre_carrera=nombre_carrera1, semestre=semestre1)


    return render(request, "asignatura.html", contexto)
