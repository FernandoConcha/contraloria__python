from django import forms
from .models import *

class RegForm(forms.Form):

    nombre = forms.CharField(max_length=100)
    descripcion = forms.CharField(max_length=100)
    estudiante_min = forms.IntegerField()
    facultad = forms.CharField(max_length=100)

    class Meta:
        modelo = Carrera
        campos = ["nombre", "descripcion", "estudiante_min", "facultad"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_descripcion(self):
        nombre = self.cleaned_data.get("descripcion")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
    def clean_estudiante_min(self):
        min = self.cleaned_data.get("estudiante_min")
        if min <= 0:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
        if min < 21:
            return min
        else:
            raise forms.ValidationError("El valor ingresado no es valido")


    def clean_facultad(self):
        nombre = self.cleaned_data.get("facultad")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")








class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = Carrera
        campos = ["nombre", "descripcion", "estudiante_min"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) >= 2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_descripcion(self):
        nombre = self.cleaned_data.get("descripcion")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_estudiante_min(self):
        min = self.cleaned_data.get("estudiante_min")
        if min <= 0:
            raise forms.ValidationError("El valor ingresado no es valido")
        if min < 21:
            return min
        else:
            raise forms.ValidationError("El valor ingresado no es valido")

    def clean_facultad(self):
        nombre = self.cleaned_data.get("facultad")
        if len(nombre) >= 2 and len(nombre) <= 100:
            return nombre
        else:
            raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

