from django.shortcuts import render
from .forms import RegForm
from .models import *

def inicioCarrera(request):
    form = RegForm(request.POST or None)
    contexto = {
        "el_formulario":
            form,
    }
    if form.is_valid():
        form_data = form.cleaned_data

        nombre1 = form_data.get("nombre")
        descripcion1 = form_data.get("descripcion")
        estudiante_min1 = form_data.get("estudiante_min")
        facultad1 = form_data.get("facultad")
        objeto = Carrera.objects.create(nombre=nombre1, descripcion=descripcion1, estudiante_min=estudiante_min1, facultad=facultad1 )


    return render(request, "carrera.html", contexto)