from django.contrib import admin

# Register your models here.
from .models import *
from .forms import *



class AdminAsignatura(admin.ModelAdmin):
        list_display = ["id","nombre_asignatura","nombre_carrera","fecha_incorporacion"]
        list_filter = ["id","nombre_carrera","nombre_asignatura"]
        form = RegModelForm

        #class Meta:
         #   model = Asignatura

admin.site.register(Asignatura, AdminAsignatura)