from django import forms

class RegModelForm(forms.ModelForm):
    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre_facultad")
        if nombre:
            if len(nombre) >= 2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")

    def clean_estudiante(self):
        min = self.cleaned_data.get("numero_min")
        if min:
            if min >= 1 and min <= 3:
                return min
            else:
                raise forms.ValidationError("El campo no tiene la cantidad de caracteres adecuados")
